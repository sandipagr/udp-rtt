package sa.udprtt.client;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;

/** 
 * Client to send and receive udp traffic
 * Computes the mean and variance of RTT time
 * 
 * @author Sandip Agrawal
 */

public class UDPClient {

    public static void main(String args[]) throws Exception {
        ArrayList<Long> time = new ArrayList<Long>();
        DatagramSocket clientSocket = new DatagramSocket();

        for (int i = 1; i < 50; i++) {
            
            InetAddress ipAddress = InetAddress.getLocalHost();
            byte[] receiveData = new byte[4096];
            byte[] sendData = new byte[4096];
            String sentence = "hellosadjsakfha";
            sendData = sentence.getBytes();
            
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipAddress, 12345);
            long time1 = System.currentTimeMillis();
            clientSocket.send(sendPacket);

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            clientSocket.receive(receivePacket);
            long time2 = System.currentTimeMillis();
            
            time.add(time2 - time1);
        }

        long sum = 0;
        double a = 0;
        double b = 0;
        for (int i = 0; i < time.size(); i++) {
            sum = sum + time.get(i);
        }
        double mean = (double) sum / time.size();
        System.out.println("Mean: " + mean);

        for (int i = 0; i < time.size(); i++) {
            a = ((double) time.get(i) - mean) * ((double) time.get(i) - mean);
            b = b + a;
        }
        double var = b / (double) (time.size() - 1);
        System.out.println("Variance = " + var);

        clientSocket.close();
    }
}
